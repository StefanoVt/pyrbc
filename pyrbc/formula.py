from collections import OrderedDict

from pyrbc.nodes.var import RBCVarNode


class Formula(object):
    """Contains a complete formula with a description of inputs and outputs"""
    @classmethod
    def build(cls, *outputs, inputs=None):
        outputs_dict = OrderedDict(enumerate(outputs))

        if inputs is not None:
            inputs_dict = OrderedDict(enumerate(inputs))
        else:
            inputs_dict = dict()
            visit = set()
            for out in outputs:
                for node in out.iter_nodes(visit):
                    if isinstance(node, RBCVarNode):
                        inputs_dict[node.name] = node
            inputs_dict = OrderedDict((name, inputs_dict[name]) for name in sorted(inputs_dict))

        return cls(inputs_dict, outputs_dict)



    def __init__(self, inputs, outputs):
        if not (isinstance(inputs, OrderedDict) and isinstance(outputs, OrderedDict)):
            raise TypeError()
        self.inputs = inputs
        self.outputs = outputs

    def __repr__(self):
        return "Formula({!r},{!r})".format(self.outputs, self.inputs)

    def __eq__(self, other):
        return self.inputs == other.inputs and self.outputs == other.outputs

    def eval(self, assgnm):
        return {k: v.eval(assgnm) for k,v in self.outputs.items()}

    def symbolic_eval(self, assgnm):
        return {k: v.symbolic_eval(assgnm) for k, v in self.outputs.items()}

    def iter_nodes(self, visited=None):
        if visited is None:
            visited = set()
        for out in self.outputs.values():
            for node in out.iter_nodes(visited):
                yield node
