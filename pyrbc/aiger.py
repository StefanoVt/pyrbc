from collections import OrderedDict
from itertools import chain
from struct import pack
from pyrbc.nodes.base import RBCEdge, RBCNode
from pyrbc import rbc
from pyrbc.formula import Formula

class IDMap(object):
    def __init__(self, formula):
        self.inputs = list(formula.inputs.values())
        self.lst = [rbc.true().node]+ self.inputs

        node_set = set(self.inputs)
        for x in chain.from_iterable(graph.iter_nodes(node_set) for graph in formula.outputs.values()):
            self.add_node(x)

    def __getitem__(self, item):
        assert isinstance(item,RBCNode)
        return self.lst.index(item)

    def __len__(self):
        return len(self.lst)

    def add_node(self, node):
        assert node not in self.lst and node.op != "var", "{!r}".format(node)
        self.lst.append(node)


    def edgeid(self, e):
        """convert node id to edge id for AIGER format"""
        ret = 2 * self[e.node]
        if e.flip:
            ret += 1
        if ret < 2: # special handling of T/F
            ret = 1 - ret
        return ret


def write_aag(f, formula):
    """Write rbc as an AIG in AIGER ASCII format file"""

    inputs, gates, ids = aig_gates(formula)
    m = len(ids)

    # AIGER header
    print ( "aag", m -1, len(inputs), 0, len(formula.outputs), len(gates), file=f)

    # inputs list
    for name in inputs:
        print (  2 * ids[rbc.var(name).node], file=f)

    # outputs list
    for graph in formula.outputs.values():
        print (  ids.edgeid(graph), file=f)

    # gates list
    for gate in gates:
        print (  " ".join(map(str, gate)), file=f)

    # names list
    for i, name in enumerate(inputs):
        if isinstance(name, str):
            print ( 'i' + str(i), name, file=f)

    for i, name in enumerate(formula.outputs.keys()):
        if isinstance(name, str):
            print('o' + str(i), name, file=f)


def aig_gates(graphs):
    """assign unique id to all nodes in the graphs

    this functions generates on the side the AIG representation in form of id triplets
    handles multiple output circuits as a list of RBCEdges"""
    gates = []
    ids = IDMap(graphs)
    inputs = ids.inputs
    node_set = set()

    # clear previous runs just in case (not needed anymore)
    # for x in chain.from_iterable(iter(graph) for graph in graphs):
    #    if hasattr(x.node, "id"):
    #        del x.node.id
    for x in graphs.iter_nodes(node_set):
        # uncomment the next lines if ids are assigned weirdly
        # assert x not in node_set
        # assert all(y.node in node_set for y in x.children)

        if x.op == "and":
            id_left = ids.edgeid(x.left)
            id_right = ids.edgeid(x.right)
            if id_left > id_right:
                id_left, id_right = id_right, id_left
            gates.append((2 * ids[x], id_left, id_right))
        elif x.op == "var":
            assert x in inputs
        else:
            #other nodes need auxiliary gates
            raise NotImplementedError("No code for exporting node " + repr(x))

    inputs = [x.name for x in inputs]
    return inputs, gates ,ids


def iter_aig_ints(f):
    """Reads integers from a AIGER binary file

    Details in http://fmv.jku.at/aiger/FORMAT"""
    while True:
        b = 99999
        i = 0
        j = 0
        r = b"\xff"
        while b >= 0x80:
            r = f.read(1)
            if not r:
                break
            b = ord(r)
            i += (0x7F & b) << j
            j += 7
        if not r:
            break
        yield i

def to_binary(x):
    ret = []
    n = x
    while n > 0x7F:
        ret.append(pack("B", 0x80+ n % 0x80))
        n = n >> 7
    ret.append(pack("b", n))
    return b''.join(ret)

def parse_aig(f):
    """Parse a binary AIGER file

    returns a list of RBCEdges"""

    head, n_gates, n_inps, _, n_outs, _ = f.readline().strip().split()

    if not isinstance(head, bytes): raise TypeError("not a binary file: {!r}".format(f))
    elif head != b'aig': raise ValueError("incorrect file header: {!r} ".format(head))

    n_gates, n_inps, n_outs = int(n_gates), int(n_inps), int(n_outs)

    # read output list, for some reason is in ASCII format
    outs = [int(f.readline()) for _ in range(int(n_outs))]

    triples = []

    # read gates triplets in binary format
    ints = iter_aig_ints(f)
    while len(triples) + n_inps < n_gates:
        try:
            a = next(ints)
            b = next(ints)
        except StopIteration:
            assert False, "missing objects"
        g = (len(triples) + n_inps + 1) * 2
        a = g - a
        b = a - b
        assert a >= 2, b >= 2
        triples.append((g, a, b))

    # read optional symbol table
    inp_names = {}
    out_names = {}

    for symbol in f:
        if symbol.startswith(b"i"):  # we read only input variable names
            pos, name = symbol[1:].strip().split(b' ', 2)
            inp_names[int(pos)] = name.decode()
        elif symbol.startswith(b"o"):
            pos, name = symbol[1:].strip().split(b' ', 2)
            out_names[int(pos)] = name.decode()
        elif symbol.startswith(b"c"):
            break
        else:
            raise ValueError("error parsing line {!r}".format(symbol))

    # start creating RBC nodes
    objs = []

    inputs = OrderedDict()
    for var_name in range(0, int(n_inps)):
        if var_name in inp_names:
            var_name = inp_names[var_name]

        var_obj = rbc.var(var_name)
        inputs[var_name] = var_obj.node
        objs.append(var_obj)

    for g, a, b in triples:
        o1 = objs[a // 2 - 1]
        o2 = objs[b // 2 - 1]
        o1 = ~o1 if a % 2 else o1
        o2 = ~o2 if b % 2 else o2
        objs.append(rbc.And(o1, o2))

    outnodes =  [~objs[x // 2 - 1] if x % 2 else objs[x // 2 - 1] for x in outs]
    outputs = OrderedDict()

    for out_name, out_node in enumerate(outnodes):
        if out_name in out_names:
            out_name = out_names[out_name]

        outputs[out_name] = out_node

    return Formula(inputs, outputs)

def write_aig(f, formula):
    """Write rbc as an AIG in AIGER binary format file"""

    inputs, gates, ids = aig_gates(formula)
    m = len(ids)

    # AIGER header
    f.write("aig {} {} {} {} {}\n".format(m - 1, len(inputs), 0, len(formula.outputs), len(gates)).encode("ascii"))

    # outputs list, for some reason in ASCII
    for graph in formula.outputs.values():
        f.write("{}\n".format(ids.edgeid(graph)).encode())

    # gates list
    for m, inp1, inp2 in gates:
        if inp2 > inp1:
            inp1, inp2 = inp2, inp1
        delta1 = m - inp1
        delta2 = inp1 - inp2
        f.write(to_binary(delta1)+ to_binary(delta2))

    # names list
    for i, name in enumerate(inputs):
        if isinstance(name, str):
            f.write('i{} {}\n'.format(i, name).encode())

    for i, name in enumerate(formula.outputs.keys()):
        if isinstance(name, str):
            f.write('o{} {}\n'.format(i, name).encode())