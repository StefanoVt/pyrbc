from collections import namedtuple, Counter
from functools import reduce
from itertools import combinations

from pyrbc import rbc
from pyrbc.aiger import aig_gates


def export_cnf_(graphs):
    """Export RBC in CNF format (list of list of literals)

    Can be fed direcly to pycosat, and is simple to write into DIMACS
    deprecated version"""

    inputs, gates, ids = aig_gates(graphs)
    m = len(ids)
    ret = []

    def prod(a, b):
        map(lambda x, y: x * y, zip(a, b))

    for gate in gates:
        # AIGER-style literals to DIMACS-style literals
        gate = map(lambda x: (x >> 1) * (1 - 2 * (x % 2)), gate)

        # each and gate from AIGER is converted into four clauses
        ret.append(prod(gate, (-1, 1, 1)))
        ret.append(prod(gate, (-1, -1, 1)))
        ret.append(prod(gate, (-1, 1, -1)))
        ret.append(prod(gate, (1, -1, -1)))

    return ret, dict((name, ids[rbc.var(name).node]) for name in inputs)

def export_cnf(graphs):
    """Export RBC in CNF format (list of list of literals)

    Can be fed direcly to pycosat, and is simple to write into DIMACS"""
    ids = [None]
    inputmap = dict()
    ret = []

    def edgeid(rbcedge):
        return ids.index(rbcedge.node) * (-1 if rbcedge.flip else 1)


    for output in graphs:
        for node in output.iter_nodes():
            if node.op == "var":
                inputmap[node.name] = len(ids)
            elif node.op == "and":
                ai = edgeid(node.left)
                bi = edgeid(node.right)
                index = len(ids)
                ret.append((index, -ai, -bi))
                ret.append((ai, -index))
                ret.append((bi, -index))
            elif node.op == "iff":
                ai = edgeid(node.left)
                bi = edgeid(node.right)
                index = len(ids)
                ret.append((-ai, -bi, -index))
                ret.append((ai, bi, -index))
                ret.append((-ai, bi, index))
                ret.append((ai, -bi, index))
            else:
                raise NotImplementedError("op not implemented:" + node.op)
            ids.append(node)

    return ret, inputmap



def parse_cnf(f):
    """Reads a DIMACS cnf file into a list of lists of literals"""
    ret = []
    for line in f:
        if line.startswith("p") or line.startswith('c'):
            continue
        lits0 = line.strip().split()
        ret.append(tuple(int(x) for x in lits0[:-1]))

    return ret


def num_vars(cnf):
    """Number of variables in a CNF list-of-lists"""
    return max(max(abs(l) for l in cl) for cl in cnf)


iff = namedtuple('iff', ('f', 'x1', 'x2'))


def decrease_width(clauses):
    """Introduce proxy variables for long CNF clauses

    (a V b V c) becomes d == (a V b) /\ (d V c)
    essentially CNF to AIG conversion, minimizing the number of new variables"""

    scoreboard = Counter()
    new_var = num_vars(clauses) + 1

    for cl in clauses:
        if type(cl) == iff:
            continue
        if len(cl) < 3:
            continue
        for cnt in combinations(cl, 2):
            scoreboard.update([cnt])

    winner = scoreboard.most_common(1)[0][0]
    ret = [iff(new_var, winner[0], winner[1])]
    for cl in clauses:
        if type(cl) == iff:
            ret.append(cl)
        elif all(x in cl for x in winner):
            ret.append((new_var,) + tuple(x for x in cl if x not in winner))
        else:
            ret.append(cl)

    return ret


def cnf_to_rbc(clauses):
    """Convert a CNF list of lists into a RBC graph

    Not really efficient but simple"""
    ret = []
    funcmap = dict()

    # create some AND2 nodes
    while max(len(x) for x in clauses if type(x) != iff) > 2:
        clauses = decrease_width(clauses)

    def getnode(x):
        if abs(x) not in funcmap:
            ret = rbc.var(str(abs(x)))
        else:
            n1, n2 = funcmap[abs(x)]
            ret = rbc.Or(getnode(n1), getnode(n2))
        return ret if x > 0 else ~ret

    # first we need to scan the AND nodes and remember them
    for el in clauses:
        if type(el) is iff:
            funcmap[el.f] = (el.x1, el.x2)

    #create a RBC for each clause
    for el in clauses:
        if type(el) is iff:
            pass  # scanned already
        elif len(el) == 2:
            ret.append(rbc.Or(getnode(el[0]), getnode(el[1])))
        elif len(el) == 1:
            ret.append(getnode(el[0]))
        else:
            assert False, "invalid constraint {}".format(repr(el))

    # merge all clauses
    return reduce(rbc.And, ret)