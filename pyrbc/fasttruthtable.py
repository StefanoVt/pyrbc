"""Module to calculate a truth table from a aig formula
probably can be organized better
"""
from pyrbc.graph import build_graph
import networkx as nx
import typing

def truth_table(aig, inputnodes):
    """Return a vector containing the output column of the truth table"""
    total = len(inputnodes)

    memo = dict((inp, index_to_table(x, total)) for x, inp in enumerate(inputnodes))

    finalflip = aig.flip
    ret = node_visit(aig.node, memo)
    if finalflip:
        ret = tuple(not x for x in ret)
    return ret

def index_to_table(index, tot):
    """get the table column for an input variable"""
    ret = tuple(True if (x >>(tot - index -1 )) % 2 else False for x in range(2<<(tot-1)))
    return ret


def node_visit(node, memo):

    if node in memo:
        return memo[node]

    le, re = node.children
    lefts = (not x if le.flip else x for x in node_visit(le.node,memo))
    rights = (not y if re.flip else y for y in node_visit(re.node,memo))

    ret = tuple(a and b for a,b in zip(lefts, rights))
    memo[node] = ret
    return ret