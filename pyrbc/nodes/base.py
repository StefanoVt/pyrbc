from pyrbc.factory import RBCFactory


class RBCEdge(object):
    """represents a proper expression as nodes cannot express negation"""
    def __init__(self, node, flip):
        self.flip = flip
        self.node = node
        self.hashmemo = hash(self.flip) + hash(self.node)

    def __hash__(self):
        """Hash value used in dicts"""
        return self.hashmemo

    def __eq__(self, oth):
        # xxx only valid when nodes are reduced, this way avoids deep recursion
        return isinstance(
            oth, RBCEdge) and self.flip == oth.flip and self.node is oth.node
    def __lt__(self, other):
        return hash(self) < hash(other)

    def __invert__(self):
        """Produce the negation of a formula

        obj.__invert__() is equivalent to ~obj"""
        return RBCEdge(self.node, not self.flip)

    def __repr__(self):
        if self.flip:
            return "~({!r})".format(self.node)
        else:
            return "({!r})".format(self.node)

    def eval(self, assignm):
        """evaluate the expression. assignm is a map to nodes or names to Boolean values"""
        if self.node in assignm: # shortcut value assignment
            val = assignm[self.node]
        else:
            val = self.node.eval(assignm)
            assignm[self.node] = val
        return self.flip ^ val

    def symbolic_eval(self, assignm):
        if self.node in assignm: # shortcut value assignment
            val = assignm[self.node]
        else:
            val = self.node.symbolic_eval(assignm)
            assignm[self.node] = val
        return ~val if self.flip else val


    def unsigned(self):
        """Remove negation if present"""
        return RBCEdge(self.node, False)

    def iter_nodes(self, visited=None):
        """Iterate over all nodes in the graph once.

        visited is the set of visited nodes, setting it explicitly is useful when iterating multiple-output functions"""
        if visited is None:
            visited = set()
        # avoid multiple visits to a node
        if self.node not in visited:
            for x in self.node.children:
                for y in x.iter_nodes(visited):
                    yield y
            yield self.node
            visited.add(self.node)


class RBCNode(object):
    """represents a logical operation over its childrens"""
    database = RBCFactory.singleton()

    def __init__(self, op, *children):
        self.op = op
        self.children = children
        self.hashmemo = hash(self.op) + sum(map(hash, children))
        #self.inputs = dict()
        #for child in children:
        #    self.inputs.update(child.node.inputs)

    @property
    def left(self):
        """left child in a binary node"""
        return self.children[0]

    @property
    def right(self):
        """right child in binary node"""
        return self.children[-1]

    @classmethod
    def new(cls, *args):
        """create a new node"""
        return cls.database.new(cls, *args)

    def __hash__(self):
        return self.hashmemo

    def __eq__(self, oth):
        """this equality takes as an assuptions that all nodes are reduced, so no two equivalent nodes are created"""
        return self is oth

    def eval(self, assignm):
        raise NotImplementedError()

    def __repr__(self):
        return "{} {} ".format(self.op, " ".join(map(repr,self.children)))