from pyrbc.factory import RBCFactory
from pyrbc.nodes.base import RBCNode, RBCEdge


class RBCITENode(RBCNode):
    """Node representing an IF-THEN-ELSE operation

    This is the only node in the library that is not symmetric, take care"""
    @staticmethod
    def add_to_db(db, a, b, c):
        """Reduction rules for ITE nodes"""
        if a == db.true():
            return b  # type: RBCEdge
        elif a == db.false():
            return c
        elif b == c:
            return b
        elif b == ~c:
            return db.Iff(a, b)

        if c == db.true() or c == db.false() or a == c or a == ~c:
            b, c = c, b
            a = ~a

        if b == db.true() or a == b:
            return db.Or(a, c)
        elif b == db.false or a == ~b:
            return db.And(~a, c)

        if a.flip:
            b, c = c, b
            a = ~a

        if b.flip:
            b, c = ~b, ~c
            flip = True
        else:
            flip = False

        key = ('ITE', a, b, c)
        if key not in db.nodedict:
            db.nodedict[key] = RBCITENode(a, b, c)

        return RBCEdge(db.nodedict[key], flip)

    def __init__(self, a, b, c):
        RBCNode.__init__(self, "Ite", a, b, c)

    def eval(self, assignment):
        res = map(lambda x: x.eval(assignment), self.children)
        if res[0]:
            return res[1]
        else:
            return res[2]

@RBCFactory.register
def Ite(db, a, b, c):
    return db.new(RBCITENode, a, b, c)