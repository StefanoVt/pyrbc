from pyrbc.factory import RBCFactory
from pyrbc.nodes.base import RBCNode, RBCEdge


class RBCTrueNode(RBCNode):
    """Constant node that represents the True value

    NB: False is represented using a negated RBCEdge and this object"""
    def __init__(self, ):
        RBCNode.__init__(self, "True")

    @staticmethod
    def add_to_db(db):
        """Ensure only a single instance (for each database) is created"""
        if not hasattr(db, 'truenode'):
            db.truenode = RBCTrueNode()

        return RBCEdge(db.truenode, False)

    @classmethod
    def true(cls):
        return cls.new(False)

    @classmethod
    def false(cls):
        return cls.new(True)

    def eval(self, assignment):
        return True

    def symbolic_eval(self, assignment):
        return self.database.true()

    def __iter__(self):
        return iter([])

@RBCFactory.register
def true(db):
    return db.new(RBCTrueNode)

@RBCFactory.register
def false(db):
    return ~db.new(RBCTrueNode)