from pyrbc.factory import RBCFactory
from pyrbc.nodes.base import RBCNode, RBCEdge


class RBCAndNode(RBCNode):
    """Node that represents an AND operation

    AIG can be loaded by using only this node, more complex nodes can be converted into And nodes (see export module)"""
    @staticmethod
    def add_to_db(db, left, right):
        """Ensure node is reduced"""

        # check trivial cases
        # NB: explicitly use thedb. true() and db.false() instead of True/False, just in case
        if left == right:
            return left
        elif left == ~right:
            return db.false()
        elif left == db.true():
            return right
        elif right == db.true():
            return left
        elif left == db.false() or right == db.false():
            return db.false()

        # order children
        if right < left:
            left, right = right, left

        if ('and', left, right) not in db.nodedict:
            ret = RBCAndNode(left, right)
            db.nodedict['and', left, right] = ret

        return RBCEdge(db.nodedict['and', left, right], False)

    def __init__(self, left, right):
        RBCNode.__init__(self, "and", left, right)

    def eval(self, assignment):
        return self.left.eval(assignment) and self.right.eval(assignment)

    def symbolic_eval(self, assignment):
        return self.database.new(RBCAndNode, self.left.symbolic_eval(assignment), self.right.symbolic_eval(assignment))

@RBCFactory.register
def And(db, a, b):
    return db.new(RBCAndNode, a, b)

@RBCFactory.register
def Or(db, a, b):
    return ~db.new(RBCAndNode, ~a, ~b)

@RBCFactory.register
def Implies(db, a, b):
    return ~db.new(RBCAndNode, a, ~b)