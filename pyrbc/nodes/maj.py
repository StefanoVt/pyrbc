from pyrbc.factory import RBCFactory
from pyrbc.nodes.base import RBCNode, RBCEdge


class RBCMajNode(RBCNode):
    """Node representing a majority ternary operator

    check http://ieeexplore.ieee.org/document/7293649/
    MIG may become useful and can be easily adapted to RBCs"""
    @staticmethod
    def add_to_db(db, x1, x2, x3):
        """Reduction rules for MAJ3 nodes"""

        if x1 == x2 or x1 == x3 or x2 == ~x3:
            return x1
        elif x2 == x3 or x1 == ~x3:
            return x2
        elif x1 == ~x2:
            return x3

        if x2 == db.true() or x2 == db.false():
            x1, x2 = x2, x1

        if x3 == db.true() or x3 == db.false():
            x1, x3 = x3, x1

        if x1 == db.true():
            return db.Or(x2, x3)
        elif x1 == db.false():
            return db.And(x2, x3)

        children = [x1, x2, x3]
        children.sort()
        if children[0].flip:
            children = map(lambda x: ~x, children)
            flip = True
        else:
            flip = False
        children = tuple(children)

        key = ('maj', children)
        if key not in db.nodedict:
            db.nodedict[key] = RBCMajNode(children)

        return RBCEdge(db.nodedict[key], flip)

    def __init__(self, children):
        RBCNode.__init__(self, "Maj", *children)

    def eval(self, assignment):
        res = map(lambda x: x.eval(assignment), self.children)
        if res[0]:
            return res[1] or res[2]
        else:
            return res[1] and res[2]

@RBCFactory.register
def Maj(db, a, b, c):
    return db.new(RBCMajNode, a, b, c)