from pyrbc.factory import RBCFactory
from pyrbc.nodes.base import RBCNode, RBCEdge


class RBCVarNode(RBCNode):
    """Node that represents a Boolean variable

    Has no children and a name attribute"""
    def __init__(self, name):
        RBCNode.__init__(self, "var")
        self.name = name
        #self.inputs[name] = self

    @staticmethod
    def add_to_db(db, name):
        """method that avoids duplicate nodes, return the object in the database if present otherwise create it"""
        if name not in db.nodedict:
            db.nodedict[name] = RBCVarNode(name)

        return RBCEdge(db.nodedict[name], False)

    def __hash__(self):
        """use the variable name as hash (possibly not necessary)"""
        return hash(self.name)

    def eval(self, assignm):
        return assignm[self.name]

    def symbolic_eval(self,assignm):
        return assignm.get(self.name, self.database.var(self.name))

    def __iter__(self):
        return iter([])

    def __repr__(self):
        return str(self.name)

@RBCFactory.register
def var(db, name):
    return db.new(RBCVarNode, name)