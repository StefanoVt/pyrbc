from pyrbc.factory import RBCFactory
from pyrbc.nodes.base import RBCNode, RBCEdge


class RBCIffNode(RBCNode):
    """Node that represents an equality or a xor operation
    """
    @staticmethod
    def add_to_db(db, left, right):
        """Ensure node is reduced. Notice the slight difference in reduction rules compared to RBCAndNode"""
        if left == right:
            return db.true()
        elif left == ~right:
            return db.false()
        elif left == db.true():
            return right
        elif left == db.false():
            return ~right
        elif right == db.true():
            return left
        elif right == db.false():
            return ~left

        # Transfer sign from children to parent
        flip = left.flip ^ right.flip
        left = left.unsigned()
        right = right.unsigned()

        if right < left:
            left, right = right, left

        if ('iff', left, right) not in db.nodedict:
            ret = RBCIffNode(left, right)
            db.nodedict['iff', left, right] = ret

        return RBCEdge(db.nodedict['iff', left, right], flip)

    def __init__(self, left, right):
        RBCNode.__init__(self, "iff", left, right)

    def eval(self, assignm):
        return self.left.eval(assignm) == self.right.eval(assignm)

@RBCFactory.register
def Iff(db, a, b):
    return db.new(RBCIffNode, a, b)

@RBCFactory.register
def Xor(db, a, b):
    return ~db.new(RBCIffNode, a, b)