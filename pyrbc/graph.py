import networkx as nx


def build_graph(graphs):
    ''' Return a graph object

    each vertex references its RBCNode and vice versa:
    rbc_node.vertex <--> graph_vertex["aignode"]
    '''
    visited = set()
    ret = nx.DiGraph()

    for node in graphs.iter_nodes(visited):
            ret.add_node(node)
            for child in node.children:
                ret.add_edge(child.node, node, flip=child.flip)

    return ret