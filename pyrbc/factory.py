
class RBCFactory(object):
    """RBC factory objects

    the class contain the database of nodes created so far, the module uses a default instance
    but new factories can be instantiated and they will not share nodes between each other.
    So far RBC objects are considered immutable, but if mutable behaviour is required, consider using
    multiple factories in order to avoid different graphs with overlapping parts
    """
    _singleton = None

    @classmethod
    def singleton(cls):
        if cls._singleton is None:
            cls._singleton = cls()
        return cls._singleton

    def __init__(self):
        self.nodedict = dict()

    def new(self, obj, *args):
        """Create a new object of type obj (and save it in the database)"""
        def clean(x):
            if x is True:
                return self.true()
            elif x is False:
                return self.false()
            return x

        args = map(clean, args)
        return obj.add_to_db(self, *args)

    @classmethod
    def register(cls, method):
        """Register a node function to the default factory.

        This allows the use of bot rbc.And() and rbc.RBCFactory().And(). Standard function can then be used with custom
        RBCFactory classes."""
        setattr(cls, method.__name__, method)
        return getattr(cls.singleton(), method.__name__)