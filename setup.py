#!/usr/bin/env python
from setuptools import find_packages, setup


with open("README.md", "r") as fh:
    long_description = fh.read()


setup(
    name='pyrbc',
    version='3.0.1',
    author="Stefano Varotti",
    author_email="stefano.varotti@unitn.it",
    description="Reduced Boolean Circuits (RBC) and And-Inverter Graph (AIG) library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/StefanoVt/pyrbc",
    packages=find_packages(exclude=['tests']),
    install_requires=['networkx'],
    tests_requires=["matplotlib"],
    test_suite='tests',
    classifiers=[
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)


