from unittest import TestCase

import pyrbc.fasttruthtable as ftt
from pyrbc.rbc import var, And
class FastTtTest(TestCase):
    def test_total(self):
        xor = And(~And(var('a'), var('b')),~And(~var('a'), ~var('b')))
        self.assertEqual((False, True, True, False),ftt.truth_table(xor, (var('a').node, var('b').node)))

