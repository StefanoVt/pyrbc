import unittest

import pyrbc.factory
import pyrbc.rbc as rbc


class RBCTests(unittest.TestCase):
    def test_creat(self):
        self.assertTrue(
            rbc.And(rbc.var("x"), rbc.var("y")).eval({"x": True, "y": True}))

    def test_unsigned(self):
        v = rbc.var("x")
        self.assertIsNot(v.unsigned(), None)
        self.assertEqual((~v).unsigned(), v.unsigned())

    def test_use_booleans(self):
        g = rbc.And(rbc.var("x"), True)
        self.assertTrue(g.eval(dict(x=True)))

    def test_multiple_dbs(self):
        db1 = pyrbc.factory.RBCFactory()
        db2 = pyrbc.factory.RBCFactory()
        f1 = db1.And(db1.var("a"), db1.var("b"))
        f2 = db1.And(db1.var("a"), db1.var("b"))
        assert f1 == f2
        f3 = db2.And(db2.var("a"), db2.var("b"))
        for n1, n2, n3 in zip(f1.iter_nodes(), f2.iter_nodes(), f3.iter_nodes()):
            self.assertTrue(n1 is n2)
            self.assertFalse(n1 is n3)


if __name__ == "__main__":
    unittest.main()
