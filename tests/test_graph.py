from unittest import TestCase
import pyrbc.graph
from os.path  import dirname
import networkx as nx
#import matplotlib.pyplot
from pyrbc.formula import Formula
from pyrbc.rbc import  And, var
import pyrbc.aiger


class TestGraph(TestCase):
    def test_build_graph(self):
        g = pyrbc.graph.build_graph(Formula.build(And(var("a"), var("b"))))
        #nx.draw(g)
        self.assertEqual(g.number_of_nodes(), 3, g.nodes(data=True))

    def test_dag(self):
        with open(dirname(__file__)+"/data/c17.aig", "rb") as f:
            edges = pyrbc.aiger.parse_aig(f)
        g = pyrbc.graph.build_graph(edges)
        #with open(dirname(__file__)+"/c17.adjlist", "w") as f:
        #    gi = nx.convert_node_labels_to_integers(g)
        #    nx.write_edgelist(gi, f)
        assert nx.is_directed_acyclic_graph(g)