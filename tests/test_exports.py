import os
import os.path
import sys
import tempfile
from itertools import product
from unittest import TestCase

import pyrbc.aiger
import pyrbc.graph
from pyrbc.rbc import *
from shutil import which
from warnings import warn
from unittest import skipIf
import os

class GraphExportTest(TestCase):
    @classmethod
    def setUpClass(cls):
        print("$PATH =",os.environ["PATH"])
        print("aigtoaig path: ", which("aigtoaig"))

    @skipIf(which("aigtoaig") is None, "AIGER tools not found")
    def test_import_export(self):

        # needs AIGER tools in $PATH
        ofn = os.path.dirname(__file__) + "/data/c17.aig"
        with open(ofn, "rb") as f:
            g = pyrbc.aiger.parse_aig(f)
        with tempfile.TemporaryDirectory() as tmpdir:
            tfn = tmpdir + "/test.aag"
            with open(tfn, "w") as f:
                pyrbc.aiger.write_aag(f, g)

            os.system("aigtoaig {0} {0}.aig".format(tfn))
            with open(tfn + ".aig", "rb") as f2:
                g2 = pyrbc.aiger.parse_aig(f2)
            #print(os.popen("aigtoaig -a {0}".format(ofn)).read())

        #pyrbc.aiger.write_aag(sys.stdout, g)
        #pyrbc.aiger.write_aag(sys.stdout, g2)

        inputs = set([x.name for x in g.iter_nodes() if hasattr(x, 'name')])

        for z in product(*([(False, True)] * len(inputs))):
            assgn = dict(zip(inputs, z))
            for o1, o2 in zip(g.outputs.values(), g2.outputs.values()):
                self.assertEqual(o1.eval(assgn), o2.eval(assgn))