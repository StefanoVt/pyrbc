from io import BytesIO, StringIO
from unittest import TestCase
import pyrbc.aiger as aig
import pyrbc.rbc as rbc
import os.path
import tempfile
from itertools import product
import matplotlib

from pyrbc.formula import Formula

matplotlib.use("WebAgg")
from matplotlib import pyplot as plt
from pyrbc.graph import build_graph
from networkx.drawing.nx_agraph import graphviz_layout
import networkx as nx



def apply_to_str(s):
    it = aig.iter_aig_ints(BytesIO(s))
    return list(it)

def allassignments(vs):
    for ass in product(*[(False, True) for _ in range(len(vs))]):
        yield dict(zip(vs, ass))

testcases = [
    (0, b'\x00'),
    (1, b'\x01'),
    (0x7F, b'\x7f'),
    (0x80, b'\x80\x01'),
    (0xFFFF, b'\xff\xff\x03'),
    (0xFFFFFFFF, b'\xff\xff\xff\xff\x0f')]


class TestAiger(TestCase):
    def test_to_binary(self):
        for i, s in testcases:
            self.assertEqual(aig.to_binary(i), s)

    def test_binary_parse(self):
        for i, s in testcases:
            self.assertEqual(apply_to_str(s), [i])

    def test_parse_unparse(self):
        ofn = os.path.dirname(__file__) + "/data/c17.aig"
        ss1 = BytesIO()
        with open(ofn, "rb") as f:
            g = aig.parse_aig(f)
        aig.write_aig(ss1, g)
        with tempfile.NamedTemporaryFile("w+b") as tf:
            aig.write_aig(tf, g)
            tf.seek(0)
            g2 = aig.parse_aig(tf)

        ss2 = BytesIO()
        aig.write_aig(ss2, g2)

        #plt.figure(1)
        #gg = build_graph(g)
        #nx.draw(gg, graphviz_layout(gg, prog='dot'), edge_cmap=plt.cm.get_cmap("Accent"),
        #        edge_color=[1 if x["flip"] else 21 for _, _, x in gg.edges_iter(data=True)],
        #        labels={x: (x.name if x.op == "var" else x.op) for x in g[0].iter_nodes()})


        #plt.figure(2)
        #g2g = build_graph(g2)
        #nx.draw(g2g, graphviz_layout(g2g, prog='dot'), edge_cmap=plt.cm.get_cmap("Accent"),
        #        edge_color=[1 if x["flip"] else 21 for _,_,x in g2g.edges_iter(data=True)],
        #        labels={x: (x.name if x.op == "var" else x.op)  for x in g2[0].iter_nodes()})
        #plt.show()
        #self.assertEqual(ss1.getvalue(), ss2.getvalue())

        for x in allassignments('12367'):
            self.assertEqual(g.eval(x), g2.eval(x))

    def test_simpleaag(self):
        ss = StringIO()
        aig.write_aag(ss, Formula.build(rbc.And(rbc.var("a"), rbc.var("b"))))
        opt1 = 'aag 3 2 0 1 1\n2\n4\n6\n6 2 4\ni0 a\ni1 b\n'
        self.assertEqual(opt1, ss.getvalue())


    def test_simpleaig(self):
        ss = BytesIO()
        orig = Formula.build(rbc.And(rbc.var("a"), rbc.var("b")))
        aig.write_aig(ss, orig)
        ss.seek(0)
        gs = aig.parse_aig(ss)
        self.assertEqual(orig, gs)
        self.assertEqual((ss.getvalue()), b'aig 3 2 0 1 1\n6\n\x02\x02i0 a\ni1 b\n')
