pyrbc: reduced boolean circuits and AIGs in Python
=========================================

##Introduction

This library allows handling of And-Inverter graphs (AIGs)
and Reduced Boolean Circuits (RBCs)

##Requirements
Networkx

##Caveats
When saving RBCs as AIGs all nodes are converted to ANDs,
when reading AIGS using RBC automatic reduction is applied
(identical circuits are collapsed)